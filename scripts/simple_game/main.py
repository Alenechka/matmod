import simple_game.gp as gp


if __name__ == "__main__":
    print("Простая игра ...\n")

    print("Обучение ...")
    winner = gp.evolve(5, 100, gp.tournament, maxgen=50)
    inp = "да"

    while inp.lower() == "да":
        print("\nНачинаем игру ...\n")
        game_winner = gp.gridgame([winner, gp.HumanPlayer()])

        if game_winner == 0:
            print("\nПобедила машина!")
        elif game_winner == 1:
            print("\nПоздравляю, вы победили!")
        else:
            print("\nНичья!")

        print("Сыграть еще раз (да/нет)?")
        inp = str(input())

        if inp and inp.lower() == 'да':
            print("Переобучить машину (да/нет)?")
            retrain = str(input())
            if retrain and retrain.lower() == 'да':
                print("Обучение ...")
                winner = gp.evolve(5, 100, gp.tournament, maxgen=50)
        else: break

    print("\nУспешное завершение ...")
