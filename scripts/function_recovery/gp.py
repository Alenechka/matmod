from random import randint, random, choice
from copy import deepcopy
from math import log


class FWrapper:
    """ Класс - обертка для математической функции. """

    def __init__(self, func, child_c, name):
        """
        :param func: математическая функция
        :param child_c: количество принимаемых параметров
        :param name: имя функции
        """

        self.func = func
        self.child_c = child_c
        self.name = name


class Node:
    """ Класс функциональных узлов дерева. Имеет потомков. """

    def __init__(self, fw: FWrapper, children):
        """
        :param fw: функция
        :param children: потомки
        """

        self.function = fw.func
        self.name = fw.name
        self.children = children

    def evaluate(self, inp):
        """
        Вычисляет значение узлов - потомков и передает эти значения в собственную функцию и вычисляет ее значение.

        :param inp: входные параметры
        :return: значение узла дерева
        """

        results = [node.evaluate(inp) for node in self.children]
        return self.function(results)

    def display(self, indent=1):
        """
        Представляет дерево в виде строки

        :param indent: отступ
        :return: дерево в виде строки
        """

        print((' ' * indent) + self.name)
        for c in self.children:
            c.display(indent + 1)


class ParamNode:
    """ Класс узлов, которые просто возвращают один из переданных в него параметров """

    def __init__(self, idx):
        """
        :param idx: порядковый номер параметра
        """

        self.idx = idx

    def evaluate(self, inp):
        """
        Возвращает параметр соответствующий индексу idx

        :param inp: входныне параметры
        :return: один из параметров
        """

        return inp[self.idx]

    def display(self, indent=1):
        print('%sp%d' % (' ' * indent, self.idx))


class ConstNode:
    """ Узел, возвращающий константу. """

    def __init__(self, v):
        """
        :param v: Значение
        """

        self.v = v

    def evaluate(self, inp):
        return self.v

    def display(self, indent=1):
        print('%s%d' % (' ' * indent, self.v))


# Инициализация используемых математических функций
add_w = FWrapper(lambda p: p[0] + p[1], 2, 'Сложение')
sub_w = FWrapper(lambda p: p[0] - p[1], 2, 'Вычитание')
mul_w = FWrapper(lambda p: p[0] * p[1], 2, 'Умножение')
if_w = FWrapper(lambda p: p[1] if p[0] > 0 else p[2], 3, 'Если больше 0')
gt_w = FWrapper(lambda p: 1 if p[0] > p[1] else 0, 2, 'Больше')

f_list = [add_w, sub_w, mul_w, if_w, gt_w]  # Список всех функций


# Не очень нужна, просто для проверки
def example_tree():
    return Node(if_w, [Node(gt_w, [ParamNode(0), ConstNode(3)]),
                       Node(add_w, [ParamNode(1), ConstNode(5)]),
                       Node(sub_w, [ParamNode(1), ConstNode(2)])])


def make_random_tree(pc, max_depth=4, fpr=0.5, ppr=0.6):
    """
    Генерирует случайное дерево

    :param pc: количество параметров
    :param max_depth: максимальная глубина ветки
    :param fpr: вероятность функционального узла
    :param ppr: вероятность узла = параметра
    :return: дерево
    """

    if random() < fpr and max_depth > 0:
        f = choice(f_list)  # выбираем случайную функцию
        children = [make_random_tree(pc, max_depth - 1, fpr, ppr) for i in range(f.child_c)]
        return Node(f, children)
    elif random() < ppr:
        return ParamNode(randint(0, pc - 1))
    else:
        return ConstNode(randint(0, 10))


def hidden_function(x, y):
    """ Функция для восстановления. Нужно для генерации исходных данных """

    return x ** 2 + 2 * y + 3 * x + 5


def build_hidden_set(n=200):
    """ Генерирует набор значений для восстановления функции """

    rows = []
    for i in range(n):
        x = randint(0, 40)
        y = randint(0, 40)
        rows.append([x, y, hidden_function(x, y)])
    return rows


def score_function(tree, s):
    """
    Функция измерения успеха. Сравнивает набор данных и дерево. Идеальный результат - 0

    :param tree: дерево
    :param s: набор данных
    :return: абсолютную погрешность решения
    """

    dif = 0
    for data in s:
        v = tree.evaluate([data[0], data[1]])
        dif += abs(v - data[2])

    return dif


def mutate(t, pc, prob_change=0.1):
    """
    Функция мутации которая изменяет поддерево

    :param t: дерево
    :param pc: количество параметров
    :param prob_change: вероятность мутации узла
    :return: мутировавшее дерево
    """

    if random() < prob_change:
        return make_random_tree(pc)  # мутируем дерево
    else:
        # Рекурсивно вызываем мутацию для потомков дерева
        result = deepcopy(t)
        if isinstance(t, Node):
            result.children = [mutate(c, pc, prob_change) for c in t.children]
        return result


def crossover(t1, t2, probswap=0.7, top=1):
    """
    Функция скрещивания

    :param t1: первое дерево
    :param t2: второе дерево
    :param probswap: вероятность скрещивания узлов
    :param top: уровень
    :return: скрещенное дерево
    """

    if random() < probswap and not top:
        return deepcopy(t2)
    else:
        result = deepcopy(t1)
        if isinstance(t1, Node) and isinstance(t2, Node):
            result.children = [crossover(c, choice(t2.children), probswap, 0) for c in t1.children]
        return result


def evolve(pc, popsize, rankfunction, maxgen=500, mutationrate=0.1, breedingrate=0.4, pexp=0.7, pnew=0.05):
    """
    Главная функция которая описывает работу алгоритма

    :param pc: количество параметров
    :param popsize: размер популяции
    :param rankfunction: функция ранжирования
    :param maxgen: максимальное количество поколений
    :param mutationrate: вероятность мутации
    :param breedingrate: вероятность скрешивания
    :param pexp: вероятность оставить плохое дерево
    :param pnew: вероятность добавления случайного нового дерева в популяцию
    :return: лучшее решение
    """

    def selectindex():
        """ Возвращает случайное число отдавая предпочтение маленьким числам. Чем меньше pexp тем больше вероятность
         маленького числа """
        return int(log(random()) / log(pexp))

    scores = None

    # Создаем случайную начальную популяцию
    population = [make_random_tree(pc) for i in range(popsize)]
    for i in range(maxgen):
        scores = rankfunction(population)
        print("Популяция №{0}. Лучший результат: {1}".format(i+1, scores[0][0]))

        if scores[0][0] == 0: break

        # Две наилучшие особи беруться всегда
        new_pop = [scores[0][1], scores[1][1]]

        # Строим следующее поколение
        while len(new_pop) < popsize:
            if random() > pnew:
                new_pop.append(mutate(crossover(scores[selectindex()][1],
                                                scores[selectindex()][1],
                                                probswap=breedingrate),
                                      pc,
                                      prob_change=mutationrate))
            else:
                new_pop.append(make_random_tree(pc))

        population = new_pop

    print("Лучшее найденное решение:")
    scores[0][1].display()
    return scores[0][1]


# Получение функции ранжирования
def get_rank_function(dataset):
    def rankfunction(population):
        scores = [(score_function(t, dataset), t) for t in population]
        scores.sort(key=lambda t: t[0])
        return scores

    return rankfunction
