import function_recovery.gp as gp


if __name__ == "__main__":
    print("Задача восстановления функции по набору данных ...\n")

    rf = gp.get_rank_function(gp.build_hidden_set())
    gp.evolve(2, 500, rf, mutationrate=0.2, breedingrate=0.1, pexp=0.7, pnew=0.1)

    print("\nУспешное завершение ...")
